##################################################
#  Nikola Kuhar                                  #
#  06/21/2017                                    #
#  version 2.0: added comments, error handling,  #
#   clears output file before appending,         #
#   and ignores lines in IP address file which   #
#   only have a newline or carriage return       #
#  Jack Cleveland IT                             #
#  Generates output file containing ports which  #
#   can be removed due to inactivity		     #
##################################################

#  sys needed for keyboard input
import sys
#  getpass needed to have secure way to enter password
from getpass import getpass
#  Needed to calculate 30 days since last flap
from datetime import datetime, timedelta
#  Official Juniper library from PyEz to establish secure SSH with switch
from jnpr.junos import Device
#  Official Juniper library from PyEz to get port info
from jnpr.junos.op.phyport import *

#  Grabs username && password. Only grabbed once.
username = input("Username: ")
password = getpass("Password: ")
#  Calculating 30 days prior to the current date.
flapDate = (datetime.now() - timedelta(days=30)).isoformat()
#  Read in IP addresses 
file = open('IP.txt', 'r')
#  Clear any data in output file
open('output.txt','w').close()
#  Iterate through IPs in IP.txt file. 
for hostname in file.read().split('\n'):
	#  Checking if line is blank
	if hostname.strip():
		#  Setting info for SSH
		dev = Device(host=hostname, user=username, passwd=password)
		#  Safely attempting to establish SSH with IP address
		try:
			print('Attempting to connect to ' + hostname + '...')
			dev.open()
		#  Error Handling if SSH fails, will fail gracefully && exit out of script
		except:
			print('ERROR! Could not establish a connection.')
			dev.close()
			sys.exit()
		#  If no error, following will run
		else:
			#  Opening output file and writing IP address
			ip = open('output.txt', 'a+')
			ip.write(hostname + '\n')
			ip.write
			ip.close()
			#  Utilizing open SSH connection with switch to grab port, up/down, && last flapped data
			ports = PhyPortTable(dev).get()
			#  Iterate through all ports
			for port in ports:
				# We only care are about ports which are currently not live. We don't want up ports listed
				if 'down' in port.oper:
					#  We also only want ports which are over 30 days since last flapped or have never been active
					if port.flapped < flapDate or 'Never' in port.flapped:
						#  If all conditions above are met, the port and its data are appended to the output file
						with open('output.txt', 'a+') as f:
							f.write('\t' + port.key + ", " + port.oper + ", " + port.flapped + '\n')
							f.close()
			#  Closing the SSH connection && moving to the next IP
			print('Closing connection with ' + hostname + '...')
			dev.close()
#  Closing IP address file
file.close()
print('Done! Exiting...')
#  Fin.