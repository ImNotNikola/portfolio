import re

def stringChanges():
    string = "String12"
    string2 = "String1234"
    string3 = "StRiNgsTrInG"
    string4 = "String-String"
    stringLength = len(string)
    stringLength2 = len(string2)
    counter = 0
    if stringLength % 4 == 0:
        print(string[stringLength::-1])
    if stringLength2 % 5 == 0:
        print(string2[:5])
    for letter in string3[:5]:
	    if letter.isupper():
		    counter += 1
	    if counter == 3:
		    print(string3.upper())
    if re.search(r'-',string4):
	    print(string4.replace('-',''))

def main():
	stringChanges()
	
if __name__ == '__main__':
	main()
	