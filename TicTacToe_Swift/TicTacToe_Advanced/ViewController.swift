//
//  ViewController.swift
//  TicTacToe_Advanced
//
//  Created by Nikola Kuhar on 3/17/19.
//  Copyright © 2019 Nikola Kuhar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var you = 0
    var computer = 0
    var youScore = 0
    var computerScore = 0
    
    
    let ROCK = 0
    let PAPER = 1
    let SCISSORS = 2
    
    @IBOutlet weak var computerImageView: UIImageView!
    @IBOutlet weak var youImageView: UIImageView!
    @IBOutlet weak var winnerLabel: UILabel!
    @IBAction func onClickRock(_ sender: UIButton) {
        self.youImageView.image = UIImage(named: "rock.jpg")
        self.you = ROCK
        self.computerChoice()
        self.winner()
        self.youImageView.isHidden = false
        self.computerImageView.isHidden = false
    }
    @IBAction func onClickPaper(_ sender: UIButton) {
        self.youImageView.image = UIImage(named: "paper.jpg")
        self.you = PAPER
        self.computerChoice()
        self.winner()
        self.youImageView.isHidden = false
        self.computerImageView.isHidden = false
    }
    @IBAction func onClickScissors(_ sender: UIButton) {
        self.youImageView.image = UIImage(named: "scissors.jpg")
        self.you = SCISSORS
        self.computerChoice()
        self.winner()
        self.youImageView.isHidden = false
        self.computerImageView.isHidden = false
    }
    @IBAction func onClickReset(_ sender: Any) {
        self.youScore = 0
        self.computerScore = 0
        self.winnerLabel.text = "\(youScore) to \(computerScore)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.computerImageView.isHidden = true
        self.youImageView.isHidden = true

    }
    
    func computerChoice() {
        self.computer = Int.random(in: 0...2)
        
        if(self.computer == ROCK) {
            self.computerImageView.image = UIImage(named: "rock.jpg")
        }
        if(self.computer == PAPER) {
            self.computerImageView.image = UIImage(named: "paper.jpg")
        }
        if(self.computer == SCISSORS) {
            self.computerImageView.image = UIImage(named: "scissors.jpg")
        }
    }
    
    func winner() {
        if(self.you == ROCK) {
            if(self.computer == ROCK) {
                self.winnerLabel.text = "It's a TIE! \(youScore) to \(computerScore)"
            }
            if(self.computer == PAPER) {
                computerScore += 1
                self.winnerLabel.text = "You Lose! \(youScore) to \(computerScore)"
            }
            if(self.computer == SCISSORS) {
                youScore += 1
                self.winnerLabel.text = "You Win! \(youScore) to \(computerScore)"
            }
        }
        if(self.you == PAPER) {
            if(self.computer == ROCK) {
                youScore += 1
                self.winnerLabel.text = "You Win! \(youScore) to \(computerScore)"
            }
            if(self.computer == PAPER) {
                self.winnerLabel.text = "It's a Tie! \(youScore) to \(computerScore)"
            }
            if(self.computer == SCISSORS) {
                computerScore += 1
                self.winnerLabel.text = "You Lose! \(youScore) to \(computerScore)"
            }
        }
        if(self.you == SCISSORS) {
            if(self.computer == ROCK) {
                computerScore += 1
                self.winnerLabel.text = "You Lose! \(youScore) to \(computerScore)"
            }
            if(self.computer == PAPER) {
                youScore += 1
                self.winnerLabel.text = "You Win! \(youScore) to \(computerScore)"
            }
            if(self.computer == SCISSORS) {
                self.winnerLabel.text = "It's a Tie! \(youScore) to \(computerScore)"
            }
        }
    }


    
}

