.data
intro: 			.asciiz	"Letter Frequency Analysis by, Nikola Kuhar \n"
prompt1: 		.asciiz	"String to analyze: \n"
colon:			.asciiz	" : "
newline:		.asciiz	"\n"


			.align 2
text:			.space 1024

			.align 2
freq:			.space 104

			.align 2
alpha:			.space 104
	   				   
.text
main:
	jal setup
	jal analyze
	jal results
	
	li $v0, 10
	syscall

setup:	
	addiu	$sp, $sp, -32
	sw	$ra, 32($sp)
	sw	$s0, 16($sp)
	sw	$s1, 20($sp)
	sw	$s2, 24($sp)
	sw	$s3, 28($sp)
	li	$s0, 0	
	li	$s2, 65	
	li	$s3, 90	
	li	$a1, 1024

loop1:	sw   	$s2, alpha($s0)	
	addi	$s0, $s0, 4
	addi	$s2, $s2, 1	
	ble	$s2, $s3, loop1
	li	$v0, 4
	la	$a0, intro		
	syscall	
	
	la	$a0, prompt1
	syscall
	
	li	$v0, 8
	la	$a0, text
	syscall

	lw 	$s0, 16($sp)
	lw	$s1, 20($sp)
	lw	$s2, 24($sp)
	lw	$s3, 28($sp)
	lw	$ra, 32($sp)
	addiu 	$sp, $sp, 32
	jr	$ra	

analyze:
	addiu	$sp, $sp, -32
	sw	$ra, 32($sp)	
	sw	$s0, 16($sp)
	sw	$s1, 20($sp)
	sw	$s2, 24($sp)
	sw	$s3, 28($sp)
	li	$s0, 0	
	li	$s1, 0	
	li	$s2, 65	
	li	$s3, 90	
		 
loop2:	lb   	$a2, alpha($s1)
	
	sw  	$a0, 0($sp)	
	sw	$a1, 4($sp)	
	sw	$a2, 8($sp)
	sw	$a3, 12($sp)
	jal	count	
	
	sw	$v0, freq($s0)
	lw   	$a0, 0($sp)	
	lw	$a1, 4($sp)	
	lw	$a2, 8($sp)
	lw	$a3, 12($sp)	
	addi	$s0, $s0, 4
	addi	$s1, $s1, 4
	ble	$s1, $s3, loop2	
	lw 	$s0, 16($sp)
	lw	$s1, 20($sp)
	lw	$s2, 24($sp)
	lw	$s3, 28($sp)
	lw	$ra, 32($sp)
	addiu 	$sp, $sp, 32
	jr	$ra	

count:
	addiu	$sp, $sp, -32
	sw	$ra, 32($sp)
	sw	$s0, 16($sp)
	sw	$s1, 20($sp)
	sw	$s2, 24($sp)
	sw	$s3, 28($sp)
	li	$v0, 0	
	li	$s0, 0 	
	li	$s1, 10	
	li	$s2, 0
	li	$s3, 0
	
loop3:	lb 	$s2, text($s0)
	beq	$s2, $s1, exit	
	bne	$s2, $a2, cont1	
	addi	$v0, $v0, 1	
	
cont1:	addi	$s3, $a2, 32 
	bne	$s2, $s3, cont2	
	addi	$v0, $v0, 1	

cont2:	addi	$s0, $s0, 1	
	j	loop3
	
exit:	lw 	$s0, 16($sp)
	lw	$s1, 20($sp)
	lw	$s2, 24($sp)
	lw	$s3, 28($sp)
	lw	$ra, 32($sp)
	addiu 	$sp, $sp, 32
	jr	$ra	

	addiu	$sp, $sp, -32	
	sw	$ra, 32($sp)
	sw	$s0, 16($sp)
	sw	$s1, 20($sp)
	sw	$s2, 24($sp)
	sw	$s3, 28($sp)
	
results:li	$s0, 0
	li	$s1, 0
	li	$s2, 90	
	li	$s3, 104

loop4:	li	$v0, 11	
	lb	$a0, alpha($s1)	
	syscall

	li	$v0, 4
	la	$a0, colon		
	syscall	
	
	li	$v0, 1		
	lb	$a0, freq($s0)
	syscall
	
	li	$v0, 4		
	la	$a0, newline				
	syscall	
	
	addi	$s0, $s0, 4	
	addi	$s1, $s1, 4	
	blt	$s1, $s3, loop4	
	lw 	$s0, 16($sp)
	lw	$s1, 20($sp)
	lw	$s2, 24($sp)
	lw	$s3, 28($sp)
	addiu 	$sp, $sp, 32
	jr	$ra