.data
	title:  .asciiz	"Math in MIPS! by, Nikola Kuhar \n Enter two numbers and Ill show you the sum,\ndifference, product, quotient and remainder \n"
	firstNumber:	.asciiz "First number: "
	secondNumber:	.asciiz "Second number: "
	byeFelicia:	.asciiz "Goodbye."
	sum:	.word	0
	difference:	.word	0
	product:	.word	0
	quotient:	.word	0
	remainder:	.word	0
	_addition:	.asciiz " + "
	_subtract:	.asciiz " - "
	_mutliply:	.asciiz " * "
	_divide:	.asciiz " / "
	_remainder:	.asciiz " R "
	_equals:	.asciiz " = "
	_newLine:	.asciiz "\n"
.text

main:
	# Print title
	li $v0, 4
	la $a0, title
	syscall

	# Get user input: firstNumber
	li $v0, 4
	la $a0, firstNumber
	syscall

	# Store firstNumber and store in $t0
	li $v0, 5
	syscall
	move $t0, $v0

	# Get user input: secondNumber
	li $v0, 4
	la $a0, secondNumber
	syscall

	# Store firstNumber and store in $t1
	li $v0, 5
	syscall
	move $t1, $v0

add:
	# $t0 + $t1 = $t2, soted in sum
	add $t2, $t0, $t1
	sw $t2, sum

subtract:
	# $t0 - $t1 = $t3, store in difference
	sub $t3, $t0, $t1
	sw $t3, difference

multiply:
	# $t0 * $t1 = $t4, store in product
	mul $t4, $t0, $t1
	sw $t4, product

divide:
	div $t5, $t0, $t1
	sw $t5, quotient
	rem $t6, $t0, $t1
	sw $t6, remainder

printResults:
	li $v0, 1
	move $a0, $t0
	syscall
	li $v0, 4
	la $a0, _addition
	syscall
	li $v0, 1
	move $a0, $t1
	syscall
	li $v0, 4
	la $a0, _equals
	syscall
	li $v0, 1
	lw $a0, sum
	syscall
	li $v0, 4
	la $a0, _newLine
	syscall
	li $v0, 1
	move $a0, $t0
	syscall
	li $v0, 4
	la $a0, _subtract
	syscall
	li $v0, 1
	move $a0, $t1
	syscall
	li $v0, 4
	la $a0, _equals
	syscall
	li $v0, 1
	lw $a0, difference
	syscall
	li $v0, 4
	la $a0, _newLine
	syscall
	li $v0, 1
	move $a0, $t0
	syscall
	li $v0, 4
	la $a0, _mutliply
	syscall
	li $v0, 1
	move $a0, $t1
	syscall
	li $v0, 4
	la $a0, _equals
	syscall
	li $v0, 1
	lw $a0, product
	syscall
	li $v0, 4
	la $a0, _newLine
	syscall
	li $v0, 1
	move $a0, $t0
	syscall
	li $v0, 4
	la $a0, _divide
	syscall
	li $v0, 1
	move $a0, $t1
	syscall
	li $v0, 4
	la $a0, _equals
	syscall
	li $v0, 1
	lw $a0, quotient
	syscall
	li $v0, 4
	la $a0, _remainder
	syscall
	li $v0, 1
	lb $a0, remainder
	syscall
	li $v0, 4
	la $a0, _newLine
	syscall

byebye:
	li $v0, 4
	la $a0, byeFelicia
	syscall

exit:
	li $v0, 10
	syscall
