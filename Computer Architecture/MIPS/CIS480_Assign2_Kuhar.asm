.data
intro:   .ascii "Nikola Kuhar. Programming assignment 2.\n"
		 .asciiz "Fibonacci Program!\n"

prompt:  .asciiz "\nEnter your name: "
hello:   .asciiz "\nHello, "
goodbye: .asciiz "\nGoodbye, "

num_prompt: .ascii  "\nHow many Fibonacci numbers should I display? "
			.asciiz "\nEnter an integer in the range [1..47]: "
num_err:    .asciiz "\nThat number was not in range."
spaces:     .asciiz "    "
nl:         .asciiz "\n"

# results stored into data memory
name:     .space   64

.text
# Section 1: Introduction

	li $v0, 4
	la $a0, intro
	syscall


	la $a0, prompt
	syscall

	li $v0, 8
	la $a0, name
	li $a1, 64
	syscall

	li $v0, 4
	la $a0, hello
	syscall

	la $a0, name
	syscall


# Section 2: Get and validate "n"

loop:
	li $v0, 4
	la $a0, num_prompt
	syscall

	li $v0, 5
	syscall

	bltz $v0, loop
	bgt $v0, 47, loop
	move $t0, $v0

# Section 3: Calculate & Print the first N Fibonacci numbers.

	li $t1, 0
	li $t2, 1

	li $a0, 0
	li $v0, 1
	syscall

	li $t4, 1
	li $v0, 4       # used for the printing_spaces part
	j print_spaces

fib_loop:

math:
	move $t3, $t1
	move $t1, $t2
	add $t2, $t3, $t2

printer:
	li $v0, 1
	move $a0, $t2
	syscall

	addi $t4, $t4, 1
	li $v0, 4
	rem $t3, $t4, 5
	beqz $t3, print_newline
	j print_spaces

print_newline:
	la $a0, nl
	j then
print_spaces:
	la $a0, spaces
then:
	syscall # print the previous part
	bgt $t0, $t4, fib_loop

# Section 4: Print a concluding message and exit

exit:
	# print goodbye
	li $v0, 4
	la $a0, goodbye
	syscall

	# print users name
	la $a0, name
	syscall

	# exit
	li  $v0, 10
	syscall
