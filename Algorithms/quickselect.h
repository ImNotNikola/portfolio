#ifndef __QUICKSELECT_H__
#define __QUICKSELECT_H__
#include <stdio.h>
#include <stdlib.h>
extern int alg2;
void swap(int *a, int *b);
int partition(int *A, int left, int right);
int quickselect(int *A, int left, int right, int k);
int select1(int *a, int n, int k);
int partition(int *a, int left, int right);
void swap(int *a, int *b);
#endif
