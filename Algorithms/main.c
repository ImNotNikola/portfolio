#include "quickselect.h"
#include"merge.h"

int alg1 = 0;
int alg2 = 0;

int *a1, *a2;
void sort(int low, int high); //Merge sort
int select1(int *A, int n, int k);

int main(int argc, char *argv[]) { 
    int i,n,k;
    printf("enter n:");
    scanf("%d", &n);
    printf("enter k:");
    scanf("%d", &k);
    srand(time(NULL));
    a1 = (int*) calloc(n, sizeof(int));
    a2 = (int*) calloc(n, sizeof(int));
    for (i = 0; i < n; i++) {
        a1[i] = rand();
        a2[i] = a1[i];
    }
    sort(0, n-1); //Merge Sort
    int kth = select1(a2, n, k); //Quick Select

	printf("Algorithm 1: %d, %d, %d, %d \n", n, k, a1[k-1], alg1);
	printf("Algorithm 2: %d, %d, %d, %d \n", n, k, kth , alg2);
}
