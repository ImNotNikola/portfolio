#ifndef __MERGE_H__
#define __MERGE_H__
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define max 1000000
extern int *a1;
extern int alg1;
int b[max];
void merging(int low, int mid, int high);
void sort(int low, int high);
#endif
