#include "quickselect.h"

int alg2;

void swap(int *a, int *b);
int partition(int *A, int left, int right);

int quickselect(int *A, int left, int right, int k) {

    int p = partition(A, left, right);
    if (p == k-1){
        return A[p];
    }
    else if (k - 1 < p){
        return quickselect(A, left, p - 1, k);
    }
    else{
		  return quickselect(A, p + 1, right, k);
    }
}

int select1(int *A, int n, int k) {

    int left = 0; 
    int right = n - 1; 
    int next = 1;

    return quickselect(A, left, right, k);
}

int partition(int *A, int left, int right) {
    int pivot = A[right], i = left, x;

    for (x = left; x < right; x++){
        if (A[x] <= pivot){
            swap(&A[i], &A[x]);
            i++;
				alg2++;
        }
    }
    swap(&A[i], &A[right]);
    return i;
}

void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}
