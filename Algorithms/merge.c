#include "merge.h"

int *a1;
int alg1;

int b[max];

void merging(int low, int mid, int high) {
   int l1, l2, i;

   for(l1 = low, l2 = mid + 1, i = low; l1 <= mid && l2 <= high; i++) {
      if(a1[l1] <= a1[l2])
         b[i] = a1[l1++];
      else
         b[i] = a1[l2++];
		alg1++;
   }
   
   while(l1 <= mid)    
      b[i++] = a1[l1++];

   while(l2 <= high)   
      b[i++] = a1[l2++];

   for(i = low; i <= high; i++)
      a1[i] = b[i];
}

void sort(int low, int high) {
   int mid;
   
   if(low < high) {
      mid = (low + high) / 2;
      sort(low, mid);
      sort(mid+1, high);
      merging(low, mid, high);
   } else { 
      return;
   }   
}


