//Libraries
#include "command.h"
#include "processhandling.h"

short mountedBool;
/*
* Append a directory name to the end of the current path name.
*/
void addPath(char *add) {
	
	char *path = getenv("PATH");
	char *newpath = malloc(strlen(path) + strlen(add) + 2);

	strcpy(newpath, path);
	strcat(newpath, ":");
	strcat(newpath, add);
	setenv("PATH", newpath, 1);

	free(newpath);
}

/*
* Deletes all instances of a directory from the PATH.
*/
void delPath(char *delete) {

	char *path = (char *)malloc((sizeof(char) * strlen(getenv("PATH"))) - (sizeof(char) * strlen(delete)));
	path[0] = '\0';
	int i = 0; char *p;
	for (i = 0, p = strtok(getenv("PATH"), ":"); p != NULL; i++, p = strtok(NULL, ":")) {
		if (strcmp(p, delete) == 0) {
			continue;
		} else {
			if (i != 0) {
				strcat(path, ":");
			}
			strcat(path, p);
		}
	}
	setenv("PATH", path, 1);
}

/*
* Prints the prompt for the shell.
*/
void prompt() {
    printf("flop: ");
}

/*
* Accept commands and returns the integer value of each command
*/
int processCommand(char* cmd){
	if (strstr(cmd, "exit") == cmd || strstr(cmd,"quit") == cmd) {
		return QUIT;
	}
	if (strstr(cmd, "path") == cmd) {
		return CHANGE_PATH;
	}
	if (strstr(cmd, "cd") == cmd) {
		return CHANGE_DIR;
	}
	return COMMAND_EXECUTE;
}


/*
* Main
*/
int main(int argc, char** argv){
   	addPath(".");

	size_t buffer = BUFFER_SIZE;
	char* command = (char*)malloc(BUFFER_SIZE);
    fmount();

	int readInput = 1;
	while (readInput) {

		prompt();
		getline(&command, &buffer, stdin);
		char* cmdPtr = command;
		while(*cmdPtr==' ')
			cmdPtr++;
		char* end = cmdPtr + strlen(cmdPtr) - 1;
		if(*end=='\n') {
			*end = '\0';
        }

        //Process commands
		switch (processCommand(cmdPtr)) {
     
			case QUIT: {
				readInput = 0;
				break;
			}

          
			case CHANGE_PATH: {
				cmdPtr+=strlen("PATH");
				while(*cmdPtr==' ')
					cmdPtr++;
				if(*cmdPtr=='+') {
                    			cmdPtr++;
					addPath(cmdPtr+1);
				} else if(*cmdPtr=='-') {
                    			cmdPtr++;
					delPath(cmdPtr+1);
				}
				
                	printf("%s\n", getenv("PATH"));
				break;
			}

           
			case CHANGE_DIR: {
				cmdPtr+=strlen("CD");
				while(*cmdPtr==' ')
					cmdPtr++;
				if(*cmdPtr=='\0') {
					chdir(getenv("HOME"));
				} else {
					if(chdir(cmdPtr)<0) {
						printf("Error: No such directory exists [\"%s\"].\n",cmdPtr);
					}
				}
				break;
			}

            	// Default to linux commands
           	 default: {
              	  process* proc = new_process(cmdPtr);
               	  execute_process(proc);
                  delete_process(proc);
                  break;
                 }
	    }
       }
	return 0;
}
