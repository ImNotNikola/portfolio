#include "command.h"
#define BUFFER_SIZE 1024
Fat12Boot boot;
Fat12Entry *entry;

int fd, counter, horizcounter, linecounter, intResult;
char result, upper, lower, counterupperhex, counterlowerhex;
char utop, umid, ubot, ltop, lmid, lbot;
int upper12, lower12;

void fmount() {
    fd = open("imagefile.img", O_RDONLY);
    mountedBool = 1;
    lseek(fd, 11, SEEK_SET); //skip 11 bytes
    read(fd,&boot.BYTES_PER_SECTOR,2);
    read(fd,&boot.SECTORS_PER_CLUSTER,1);
    read(fd,&boot.RESERVED_SECTORS,2);
    read(fd,&boot.NUM_OF_FAT,1);
    read(fd,&boot.MAX_ROOT_DIRS,2);
    entry = (Fat12Boot *)malloc(sizeof(Fat12Boot) * boot.MAX_ROOT_DIRS);
    read(fd,&boot.TOTAL_SECTORS,2);
    lseek(fd, 1, SEEK_CUR);
    read(fd,&boot.SECTORS_PER_FAT,2);
    read(fd,&boot.SECTORS_PER_TRACK,2);
    read(fd,&boot.NUM_OF_HEADS,2);
    lseek(fd, 11, SEEK_CUR);
    read(fd,&boot.VOLUME_ID,4);
    read(fd,&boot.VOLUME_LABEL,11);
    
    //Move to beginning of ROOT DIRECTORY
    lseek(fd, ((boot.NUM_OF_FAT * boot.SECTORS_PER_FAT) + 1) * boot.BYTES_PER_SECTOR, SEEK_SET);
    
    for (int i = 0; i < boot.MAX_ROOT_DIRS; i++) {
        read(fd,&entry[i].FILENAME,8);
        read(fd,&entry[i].EXT,3);
        read(fd,&entry[i].ATTRIBUTES,1);
        read(fd,&entry[i].RESERVED,2);
        read(fd,&entry[i].CREATION_TIME,2);
        read(fd,&entry[i].CREATION_DATE,2);
        read(fd,&entry[i].LAST_ACCESS_DATE,2);
        lseek(fd, 2, SEEK_CUR); //skip 2 bytes
        read(fd,&entry[i].MODIFY_TIME,2);
        read(fd,&entry[i].MODIFY_DATE,2);
        read(fd,&entry[i].START_CLUSTER,2);
        read(fd,&entry[i].FILE_SIZE,4);
    }
    printf("Mount successful.\n");
}

void fumount() {
    if (mountedBool == 1) {
        mountedBool = 0;
        close(fd);
        boot = (Fat12Boot){0};
        entry = (Fat12Entry *){0};
        printf("Unmount successful.\n");
    } else printf("There is no file to unmount.\n");
}

void help() {
    printf("The available commands are:\n");
    printf("help                    - display a list of available commands.\n");
    printf("fmount <filename.img>   - mount disk using FileName.IMG as argument \n");
    printf("fumount                 - unmount current disk. \n");
    printf("structure               - list the structure of the floppy disk image.\n");
    printf("traverse                - list the contents in the root directory.\n");
    printf("showsector <sector_num> - show the content of the given sector.\n");
    printf("showfat                 - show the content of the FAT table.\n");
    printf("quit                    - exit the floppy shell \n");
}

void traverse(int l) {
        if (l == 1) {
            printf("    *****************************\n");
            printf("    ** FILE ATTRIBUTE NOTATION **\n");
            printf("    **                         **\n");
            printf("    ** R ------ READ ONLY FILE **\n");
            printf("    ** S ------ SYSTEM FILE    **\n");
            printf("    ** H ------ HIDDEN FILE    **\n");
            printf("    ** A ------ ARCHIVE FILE   **\n");
            printf("    *****************************\n");
            printf("\n");
            for (int i = 0; i < boot.MAX_ROOT_DIRS; i++) {
                if (entry[i].FILENAME[0] != 0x00 && entry[i].START_CLUSTER != 0) {
                    char attr[6] = {'-', '-', '-', '-', '-'};
                    unsigned char a = entry[i].ATTRIBUTES[0];
                    if (a == 0x01)
                        attr[0] = 'R';
                    if (a == 0x02)
                        attr[1] = 'H';
                    if (a == 0x04)
                        attr[2] = 'S';
                    if (a == 0x20)
                        attr[5] = 'A';
                    if (a == 0x10) {
                        for (int j = 0; j < 6; j++)
                            attr[j] = '-';
                    }
                    
                    if (entry[i].ATTRIBUTES[0] == 0x10) {
                        printf("%.6s    %d %d       < DIR >      /%.8s                 %d\n", attr, entry[i].CREATION_DATE, entry[i].CREATION_TIME, entry[i].FILENAME, entry[i].START_CLUSTER);
                        printf("%.6s    %d %d       < DIR >      /%.8s/.                 %d\n", attr, entry[i].CREATION_DATE, entry[i].CREATION_TIME, entry[i].FILENAME, entry[i].START_CLUSTER);
                        printf("%.6s    %d %d       < DIR >      /%.8s/..                 %d\n", attr, entry[i].CREATION_DATE, entry[i].CREATION_TIME, entry[i].FILENAME, 0);
                    } else {
                        printf("%.6s    %d %d       %lu      /%.8s.%.3s                 %d\n", attr, entry[i].CREATION_DATE, entry[i].CREATION_TIME, entry[i].FILE_SIZE, entry[i].FILENAME, entry[i].EXT, entry[i].START_CLUSTER);
                    }
                }
            }
            
       } else {
            for (int i = 0; i < boot.MAX_ROOT_DIRS; i++) {
                if (entry[i].FILENAME[0] != 0x00 && entry[i].START_CLUSTER != 0) {
                    if (entry[i].ATTRIBUTES[0] == 0x10) {
                        printf("/%.8s                       < DIR >\n", entry[i].FILENAME);
                        printf("/%.8s/.                     < DIR >\n", entry[i].FILENAME);
                        printf("/%.8s/..                    < DIR >\n", entry[i].FILENAME);
                    } else {
                        printf("/%.8s.%.3s\n", entry[i].FILENAME, entry[i].EXT);
                        
                    }
                }
           }
        }
    }
void structure() {
	printf("\n     number of FAT:                  %d\n", boot.NUM_OF_FAT);
	printf("     number of sectors used by FAT:  %d\n", boot.SECTORS_PER_FAT);
	printf("     number of sectors per cluster:  %d\n", boot.SECTORS_PER_CLUSTER);
	printf("     number of ROOT Entries:         %d\n", boot.MAX_ROOT_DIRS);
	printf("     number of bytes per sector:     %d\n", boot.BYTES_PER_SECTOR);
	short num = boot.SECTORS_PER_FAT + 1;
	printf("      ---Sector #---     ---Sector Types---\n");
	printf("           0                   BOOT\n");
	printf("        01 -- 0%d               FAT1\n", (boot.SECTORS_PER_FAT));
	printf("        %d -- %d               FAT2\n", num, (num + boot.SECTORS_PER_FAT - 1));
	printf("        %d -- 32               ROOT DIRECTORY\n", (num + boot.SECTORS_PER_FAT));
}

void showsector(int sectornumber) {
    unsigned char buf[BUFFER_SIZE];
    lseek(fd, sectornumber * 512, SEEK_SET);
    read(fd, buf, 512);
    int i=0;
    counter = 0;
    horizcounter = 16;
    
    printf("\n\t0\t1\t2\t3\t4\t5\t6\t7\t8\t9\tA\tB\tC\tD\tE\tF");
    
    for (i = 0; i < 512; i++) {
        if (horizcounter == 16) {
            horizcounter = 0;
            counterupperhex = int2upperhex(counter);
            counterlowerhex = int2lowerhex(counter);
            printf("\n%c%c", counterupperhex, counterlowerhex);
            counter++;
        }
        upper = int2upperhex(buf[i]);
        lower = int2lowerhex(buf[i]);
        if (upper == '0') printf("\t%c", lower);
        else printf("\t%c%c", upper, lower);
        horizcounter++;
    }
    printf("\n");
}


void showfat() {
    unsigned char buf[BUFFER_SIZE];
    lseek(fd, 1 * 512, SEEK_SET);
    read(fd, buf, 256);
    int i = 0;
    counter = 16;
    linecounter = 0;
    
    printf("\n      0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F");
    
    for (i = 0; i < 384; i = i + 3) {
        if (counter == 16) {
            printf("\n%X  ", linecounter);
            if (linecounter < 256) printf(" ");
            counter = 0;
            linecounter = linecounter + 16;
        }
        upper12 = char2upper12(buf[i], buf[i + 1]);
        lower12 = char2lower12(buf[i + 1], buf[i + 2]);
        utop = int2tophex(upper12);
        umid = int2midhex(upper12);
        ubot = int2bothex(upper12);
        ltop = int2tophex(lower12);
        lmid = int2midhex(lower12);
        lbot = int2bothex(lower12);
        if (i != 0) {
            if (ltop == '0' && utop == '0' && umid == '0') printf("FREE ");
            else if (ltop == '0' && utop == '0') printf("   %c ", umid);
            else if (ltop == '0') printf("  %c%c ", utop, umid);
            else printf(" %c%c%c ", ltop, utop, umid);
            
            if (lmid == '0' && lbot == '0' && ubot == '0') printf("FREE ");
            else if (lmid == '0' && lbot == '0') printf("   %c ", ubot);
            else if (lmid == '0') printf("  %c%c ", lbot, ubot);
            else printf(" %c%c%c ", lmid, lbot, ubot);
        }else printf("           ");
        counter = counter + 2;
    }
    printf("\n");
}

//Below are functions used exclusively for showsector()

char int2upperhex(int input) {
    result = input / 16;
    if (result < 10) return result + 48;
    else if (result < 16) return result + 55;
    else return 0;
}

char int2lowerhex(int input) {
    result = input % 16;
    if (result < 10) return result + 48;
    else if (result < 16) return result + 55;
    else return 0;
}

//Below are functions used exclusively for showfat()

int char2upper12(unsigned char one, unsigned char two) {
    intResult = (one << 4) + (two / 16);
    return intResult;
}

int char2lower12(unsigned char two, unsigned char three) {
    intResult = ((two % 16) << 8) + (three);
    return intResult;
}

char int2tophex(int input) {
    result = (input / 16) / 16;
    if (result < 10) return result + 48;
    else if (result < 16) return result + 55;
    else return 0;
}
char int2midhex(int input) {
    result = (input / 16) % 16;
    if (result < 10) return result + 48;
    else if (result < 16) return result + 55;
    else return 0;
}
char int2bothex(int input) {
    result = input % 16;
    if (result < 10) return result + 48;
    else if (result < 16) return result + 55;
    else return 0;
}