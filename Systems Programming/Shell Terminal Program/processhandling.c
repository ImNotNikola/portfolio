# define BUFSIZE 1024
#define TOKEN_DELIMITER
char *flopsh_read_line(void) {
    char *line = NULL;
    ssize_t bufsize = 0;
    getline(&line, &bufsize, stdin);
    return line;
}

char **flopsh_split_line(char *line) {
    int bufsize = BUFSIZE, position = 0;
    char **tokens = malloc(bufsize *sizeof)char*));
    char **token;
    
    if (!tokens) {
        fprintf(stderr, "flop: allocation error\n");
        exit(EXIT_FAILURE);
    }
    token = strtok(line, TOKEN_DELIMITER);
    while (token != NULL) {
        tokens[position] = token;
        position++;
        if (position >= bufsize) {
            bufsize += BUFSIZE;
            tokens = realloc(tokens, bufsize * sizeof(char*));
            if (!tokens) {
                fprintf(stderr, "flop: allocation error\n");
                exit(EXIT_FAILURE);
            }
        }
        token = strtok(NULL, TOKEN_DELIMITER);
    }
    tokens[position] = NULL;
    return tokens;
}

int flopsh_launch(char **args)
{
    pid_t pid, wpid;
    int status;
    pid = fork();
    
    if (pid == 0) {
        if (execvp(args[0], args) == -1) perror("flop: ");
        exit(EXIT_FAILURE);
    } else if (pid < 0) perror("flop: ");
    else {
        do {
            wpid = waitpid(pid, &status, WUNTRACED);
        } while (!WIFEXITED(status) && !WIFSIGNALED(status));
    }
    return 1;
}