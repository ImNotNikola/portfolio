#ifndef __NODE_H__
#define __NODE_H__
struct mynode {
    int const value;
    struct mynode *next;
    struct mynode *prev;
};
struct mynode* quickSort(struct mynode *);
void freeMemory(struct mynode *);
void previousLinks(struct mynode *);
void printList(struct mynode *);
#endif