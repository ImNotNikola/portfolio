#include <stdlib.h>
#include <stdio.h>
#include "node.h"

struct mynode* quickSort(struct mynode *headNode)
{
    if (headNode->next == NULL) return headNode;
    
	struct mynode *currentLocation = headNode;
	struct mynode *leftStartPoint = (struct mynode *)malloc(sizeof(struct mynode));
	struct mynode *rightStartPoint = (struct mynode *)malloc(sizeof(struct mynode));
	struct mynode *leftNode = leftStartPoint;
	struct mynode *rightNode = rightStartPoint;
    
    int pivot = headNode->value;
	
	while (currentLocation->next != NULL) {
	    currentLocation = currentLocation->next;
		if (currentLocation->value < pivot) {
			leftNode->next = (struct mynode *)malloc(sizeof(struct mynode));
			leftNode = leftNode->next;
			*(int *)&leftNode->value = currentLocation->value;
		} else {
			rightNode->next = (struct mynode *)malloc(sizeof(struct mynode));
			rightNode = rightNode->next;
			*(int *)&rightNode->value = currentLocation->value;
		}
	}
    
	*(int *)&rightStartPoint->value = pivot;
	if (leftNode != leftStartPoint) {
		leftNode->next = rightStartPoint;
		leftStartPoint = quickSort(leftStartPoint->next);
		previousLinks(leftStartPoint);
		return leftStartPoint;
	} else {
		rightStartPoint->next = quickSort(rightStartPoint->next);
		previousLinks(rightStartPoint);
		return rightStartPoint;
	}
}

void previousLinks(struct mynode *node)
{
	struct mynode *prevNode = (struct mynode *)malloc(sizeof(struct mynode));	
	while (node->next != NULL) {
		prevNode = node;
		node = node->next;
		node->prev = prevNode; 
	}
}

void printList(struct mynode *headNode)
{
    struct mynode *node = headNode;
    int i;
    for (i=1; node->next != NULL; i++) {
	printf("%d<==>",node->value);
        node = node->next;
    }
    printf("%d",node->value);
    printf("\n");
}

void freeMemory(struct mynode *headNode)
{
    struct mynode *temp, *node = headNode;
    while (node) {
        temp = node;
        node = node->next;
        free(temp);
    }
    headNode = NULL;
}
