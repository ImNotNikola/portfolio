#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#define DRIVER_AUTHOR "Nikola Kuhar <NKuhar@me.com>"
#define DRIVER_DESCRIPTION "My First Kernel Module"

static int __init hello_init(void)
{
    printk(KERN_INFO "Hello, world\n");
    return 0;
}

static void __exit hello_exit(void)
{
    printk(KERN_ALERT "Goodbye\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESCRIPTION);
MODULE_SUPPORTED_DEVICE("Test Device");
