//
//  AppDelegate.h
//  TicTacToe
//
//  Created by Nikola Kuhar on 3/17/19.
//  Copyright © 2019 Nikola Kuhar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

