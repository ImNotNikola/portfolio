//
//  ViewController.h
//  ITCS2825_Lab7_Kuhar
//
//  Created by C2065 IOS 2 J Kozlevcar on 3/21/14.
//  Copyright (c) 2014 C2065 IOS 2 J Kozlevcar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *computerImageView;
@property (weak, nonatomic) IBOutlet UIImageView *youImageView;
@property (weak, nonatomic) IBOutlet UILabel *winnerLabel;
- (IBAction)onClickPaper:(id)sender;
- (IBAction)onClickScissors:(id)sender;
- (IBAction)onClickRock:(id)sender;
@property int you;
@property int computer;
@property int youScore;
@property int computerScore;
@end
